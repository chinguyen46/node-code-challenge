const express = require('express');
const app = express();
const sqlite3 = require('sqlite3').verbose();
app.use(express.static("public"));

const db = new sqlite3.Database ('./locations.db', (err) => {
    if (err) { return console.error(err.message);
    }else{
    console.log('Connected to SQlite database...');
    app.listen(3000, () => {
        console.log('Server started...');
    });
    }
  });


app.get('/', (req, res) => {
  res.render('./index.html');
});
 
app.get('/locations', (req, res) =>{
    let searchQuery = req.query.search + '%'; 
    let sql = 'SELECT name, latitude, longitude FROM locations WHERE name LIKE ? ORDER BY ? LIMIT 10';

    if(req.query.search.length >= 2){
        db.all(sql, [searchQuery, searchQuery],  (err, rows) => {
            if(err){
                console.log(err);
                return res.status(400).send('400 - bad request');
            }else{
                res.json(rows);
            }
        });     
    }else{
        console.log('need more than two letters');
        return res.status(400).send('400 - bad request');
    }   
});

// db.close(err => {
// if (err) {return console.error(err.message);}
// console.log('Closed the database connection.');
// });







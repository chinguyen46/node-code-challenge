import React from 'react';

const SearchBar = (props) => {

    
    return (
        <div className="">
            <form className="form-inline">
                <div className="field">
                    <label className="text-center">Type in a location!</label>
                    <input class="form-control"
                        type="text" 
                        value = {props.value} 
                        onChange = {props.onChange}
                    />
                </div>
            </form>
        </div>
    )
}

export default SearchBar; 
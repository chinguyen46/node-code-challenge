import React from 'react';
import SearchBar from './SearchBar.js';
import axios from 'axios';
import LocationList from './LocationList';
import { debounce } from 'lodash';

class App extends React.Component{

    state = {searchTerm: '', locations: [], showWarning: false};

    onTermSubmit = async (searchTerm) =>{
        const newState = {};

        try{
           var response = await axios.get('/locations/?search=' + searchTerm); 
           newState.showWarning = false;
           newState.locations = response.data;        
        } catch (err){
            newState.showWarning = true;
            newState.locations = [];
            console.log(err);
        }
		this.setState(newState);
    };
    
    onInputChange = (event) =>{
        this.setState({ searchTerm: event.target.value }, () => {
            this.onFormSubmit(event);
        });
	};

	onFormSubmit = debounce(() =>{
		this.onTermSubmit(this.state.searchTerm);
	}, 500);

    render(){
        return(
            <div className="container">
                <SearchBar onChange={this.onInputChange} value={this.state.searchTerm}/>
                <div>
                    <LocationList locations={this.state.locations} />
                </div>
                { this.state.showWarning ? <div>You need to type in more than one character</div> : null }
            </div>
        )
    }
};

export default App; 
import React from 'react';

const LocationList = props =>{
	  
	const renderedLocations = props.locations.map(location => location);
	
	return (
	<div className="text-left">
        <ul className="list-group">
            {renderedLocations.map(location => (<li className="list-group-item">| {location.name} | latitude: {location.latitude} longitude: {location.longitude} |</li>))}
        </ul>
	</div>
	)
};


export default LocationList; 
# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Output: 
2
1

Answer Q1: 
setTimeOut() is a method which delays the execution of code inside the callback function. In this case, the function will print '1' after 100ms. Hence, although setTimeout is called first, the specified delay time of 100ms means the code will be executed asynchronously -- after console.log('2') is called. 

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

Output: 
10
9
8
7
6
5
4
3
2
1
0

Answer Q2:
foo(0) is called and executed the function foo(d). foo(d) checks if d < 10, if it is not then the function is recursively called and increments foo(d+1). The recursive pattern occurs until d >= 10. This is why the output decrements from 10 - 0 (as opposed to 0-10). It is only when d=10 that the function is no longer called recursively and console.log(d) executes. 

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

Answer Q3:
The potential problem with this code is that if the value 0 or null is passed in as an argument it won't be printed. This is because the logical OR || operator adheres to the following condition: If the first value is truthy that gets returned. Otherwise, the second value gets returned. This logic is problematic for the values 0/null because they are falsy. Meaning, calling foo(0)/foo(null) will both evaluate to 5 as they will be passed through as falsy arguments. To resolve this we can instead check if the values are undefined before assinging a value (as opposed to truthy/falsy inherent in logical operators).

Alternative:
function foo(d){
  if(d === undefined){
    d = 5;
  }
  console.log(d);
};


Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Answer Q4: 
This is a function returning a function -- in JS 'closures' allow inner functions to acess the variables/parameteres of its outer function. In this case, foo(1) -- the outer function, is first stored into variable bar. When bar(2) is executed, foo(1) is evaluated and returns an anon function(2), which then returns 3 (1 + 2). The anon function has access to parameter a thanks to closures. 

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```
Answer Q5: 
The function is named 'double' so I assume it is used to x2 a value (a) that is passed in as an argument after 100ms. In order to use the function you can either use a named or anon function as callback. 

Anonymous: 
double(5, function(a){
  console.log(a);
});

Named:
function callback(a){
  console.log(a);
};

double(5, callback);
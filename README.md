# Documentation

## Installation & Running

To start the project cd into the directory 'chi-node-challenge' and execute the following commands:
~~~
npm install
npm run build
node index.js
~~~

*npm i* - will install all necessary dependencies.
*npm run build* - will build the react components.
*node index.js* - will start the server with the components rendered on the homepage.

## Endpoints

The endpoints are in the index.js file of the root folder.
~~~
GET /
GET /locations
~~~
*GET /* - renders the index.html page when localhost:3000 is requested.
*GET /locations* - takes in a query from user input and then matches query with data in the SQLite database. Express will then allow us to return a JSON response with data from the db if certain conditions are met i.e. if(req.query.search.length >= 2).

## Components

#### App
**onTermSubmit** - function that takes in this.state.searchTerm and makes the GET request to /locations. 
**onInputChange** - function that monitors for value changes in the SearchBar input and sets this.state.searchTerm accordingly.
**onFormSubmit** - function that calls onTermSubmit() everytime value changes in SearchBar occurs. This method also ensures page doesn't reload with every input change. Lastly, method using debounce to ensure database cannot be spammed with requests. 

#### SearchBar
Render a search bar on the browser which passes user input back to App component. Also has onChange event handler which will call the onInputChange method passed down from App component every time there is a value change in input.

#### LocationList
Takes in the location data from the App component as props. It then iterates through the array and returns new renderedLocations array. renderedLocations is then iterated through and the relevent information is rendered on the browser. 


